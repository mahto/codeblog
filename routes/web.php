<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', function(){

    return view('admin.index');
});

Route::get('/post/{id}', ['as'=>'home.post', 'uses'=>'AdminPostsController@post']);


//Route group

Route::group(['middleware' => 'admin'], function(){

    Route::resource('admin/users', 'AdminUsersController');

    Route::get('/admin/users', 'AdminUsersController@index')->name('admin.users.index');

    Route::get('/admin/users/create', 'AdminUsersController@create')->name('admin.users.create');

    Route::get('/admin/users/{id}/edit/', 'AdminUsersController@edit')->name('admin.users.edit');

    //Routes for Posts

    Route::resource('admin/posts', 'AdminPostsController');

    Route::get('admin/posts' , 'AdminPostsController@index')->name('admin.posts.index');

    Route::get('admin/posts/create', 'AdminPostsController@create')->name('admin.posts.create');

    Route::get('admin/posts/{id}/edit', 'AdminPostsController@edit')->name('admin.posts.edit');


    //Routes for Categories

    Route::resource('admin/categories', 'AdminCategoriesController');

    Route::get('admin/categories' , 'AdminCategoriesController@index')->name('admin.categories.index');

    Route::get('admin/categories/create' , 'AdminCategoriesController@create')->name('admin.categories.create');

    Route::get('admin/categories/{id}/edit' , 'AdminCategoriesController@edit')->name('admin.categories.edit');

    //Routes for Media

    Route::resource('admin/media', 'AdminMediasController');

    Route::get('admin/media', ['as'=>'admin.media.index','uses'=>'AdminMediasController@index']);

    Route::get('admin/media/create', ['as'=>'admin.media.create','uses'=>'AdminMediasController@create']);

    //Routes for Comments

    Route::resource('admin/comments', 'PostCommentController');

    Route::resource('admin/comment/replies', 'CommentRepliesController');

    Route::get('admin/comments' , 'PostCommentController@index')->name('admin.comments.index');

    Route::get('admin/comments/{id}' , 'PostCommentController@show')->name('admin.comments.show');

    Route::get('admin/comments/replies/{id}' , 'CommentRepliesController@show')->name('admin.comments.replies.show');






});


//Route for logout functionality

Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );



Route::group(['middleware' => 'auth'], function(){

    Route::post('comment/reply', 'CommentRepliesController@createReply');

});









