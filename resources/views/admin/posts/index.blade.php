@extends('layouts.admin')

@section('content')

    <h1>Posts</h1>

    <table class="table">
        <thead>
            <tr>
                <th>Photo</th>
                <th>Id</th>
                <th>User</th>
                <th>Category</th>
                <th>Title</th>
                <th>Body</th>
                <th>Post</th>
                <th>Comment</th>
                <th>Created_at</th>
                <th>Updated_at</th>
            </tr>
        </thead>
        <tbody>

        @if($posts)

            @foreach($posts as $post)
                <tr>
                    <td><img height = "100" width = "100" src = "{{$post->photo ? $post->photo->file : "http://placehold.it/100X100"}}" alt=" "></td>
                    <td>{{$post->id}}</td>
                    <td><a href = "{{ route('admin.posts.edit', $post->id) }}"> {{$post->user->name}}</a></td>
                    <td>{{$post->category ? $post->category->name : "Uncategorized" }}</td>
                    <td>{{$post->title}}</td>
                    <td>{{str_limit($post->body, 7)}}</td>
                    <td><a href="{{ route('home.post', $post->slug) }}" >View Post</a></td>
                    <td><a href="{{ route('admin.comments.show', $post->id) }}" >View Comment</a></td>
                    <td>{{$post->created_at->diffForHumans()}}</td>
                    <td>{{$post->updated_at->diffForHumans()}}</td>
                </tr>
            @endforeach

        @endif

        </tbody>
     </table>

    <div class = "row">

        <div class = "col-sm-6 col-sm-offset-5">
            {{$posts->render()}}
        </div>

    </div>

@stop